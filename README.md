# 💡 MMI Smart

Ce plugin jQuery vous permet d'ajouter de la documentation à vos exercices en langage HTML. Il a pour but d'informer directement l’étudiant avec des informations précises sur l'utilisation des balises HTML (CSS, JavaScript et PHP dans une future mise à jour). 🔗 [Voir la DEMO](https://jsfiddle.net/emiliencode/fmwnye4h/21/).

<img src="https://emchaptard.github.io/data/mmi-smart.png" alt="Screenshot MMI Smart" width="60%">


## 🧐  Pourquoi ?

### Apprentissage

Les étudiants peuvent ajouter MMI Smart puis le bouton aide en suivant le guide d'instalation juste en dessous et gagner du temps dès le début !
Les enseignants et intervenants en intégrant MMI Smart et le bouton aide HTML, PHP, CSS... en fontion de l'exercice demandé.
Ou même encore les lycéens en journée d'immersion à l'IUT, pour être au plus près de la formation MMI Le Puy.<br>
Des initiations pourront aussi être proposées lors des portes ouvertes.

Bref, tout le monde peut avoir besoin de MMI Smart ! Génial !

### Autonomie

Avant même que l’étudiant se pose la question, il obtient donc toute les réponses à ces questions directement. Un bouton 'Aide' lui permettra de visualiser la documentation et les balises adapté à l'exercice d'initiation.

<img src="https://emchaptard.github.io/data/balises.png" alt="Screenshot affichages des balises" width="60%">

### Simple d'utilisation

Les étudiants ont la possibilité de cliquer directement sur les informations surligner (les codes HTML, liens...). Le plugin ajoutera le ou les informations cliqués directement dans la zone de texte pour une utilisation encore plus simple.


# 🎛️  Installation


## 📁  Aucun projet existant

### Cloner le projet

Si vous n'avez pas de projet existant :

Dans un Terminal exécuter les commandes suivantes selon votre choix puis adaptez l'exercice comme vous le souhaitez.<br>
Si vous avez besoin d'aide pour cette étape consulter la 🔗[documentation](https://help.github.com/en/github/creating-cloning-and-archiving-repositories/cloning-a-repository).

### Via HTTPS

`git clone https://gitlab.com/emilienchaptard/mmi-smart.git`

### Via SSH

`git clone git@gitlab.com:emilienchaptard/mmi-smart.git`
<br><br>

## 📁  Projet déjà existant

Si vous avez déjà un projet existant, suivez les instructions suivante.


### Ajoutez le fichier jquery.mmi-smart.js ou jquery.mmi-smart.min.js

Ajouter simplement cette ligne de code avant la fermeture de la balise `<body>` de votre fichier HTML.

```HTML
<script src="https://emchaptard.github.io/data/jquery.mmi-smart.js"></script>
```
Ou cette ligne pour la version minifié (meilleure optimisation et chargement plus rapide)

```HTML
<script src="https://emchaptard.github.io/data/jquery.mmi-smart.min.js"></script>
```

Si vous le souhaitez, télécharger le fichier pour ne dépendre d'aucuns serveurs 😉 Puis ajoutez la ligne correspondante avant la fermeture de la balise `<body>` de votre fichier HTML.<br>
Attention à changer le nom du dossier dans lequel se trouve le fichier (en général un dossier 'JS').

🔗 [Téléchargement ici](https://gitlab.com/emilienchaptard/mmi-smart/-/blob/master/js/jquery.mmi-smart.js)

Puis ajoutez cette ligne

```HTML
<script src="---votre-dossier---/jquery.mmi-smart.js"></script>
```
<br>

🔗 [Téléchargement ici (version min pour une meilleure optimisation)](https://gitlab.com/emilienchaptard/mmi-smart/-/blob/master/js/jquery.mmi-smart.min.js)

Pour la version minifié ajoutez cette ligne

```HTML
<script src="---votre-dossier---/jquery.mmi-smart.min.js"></script>
```

### Création du `<textarea>`

Ajouter la balise `<textarea>` dans votre fichier HTML, elle permettra aux étudiants de faire l'exercice que vous proposez, des alertes permettront d'aider les étudiants lors de la première balise (appui touche clavier <).

```HTML
<textarea placeholder="C'est parti ! commencer à coder ici" id="texte" style='width: 70%; height: 200px;' spellcheck="false"></textarea>
```

### Création du `<button>`

Ajoutez les boutons d'aides dans votre fichier HTML, il permmettront de charger la documentation sur les balises (en ligne).

```HTML
<button id="bouton">Besoin d'aide avec les balises ?</button>
```

### Ajouter JQuery

⚠️   N'oubliez pas d'ajouter JQuery (Requiert la version 3.4.1)

```HTML
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
```

### Ajouter le style CSS

Ce code permmettra de faire disparaitre le ou les boutons après le click. Ajoutez ce code dans votre fichier CSS.

```CSS
.suppr {
  visibility: hidden;
  opacity: 0;
  transform: scale(.5), ;
}
```

## 🧭  Compatibilité

Nécessite le JavaScript activé dans les réglages de votre navigateur.<br><br>
✅ Compatible avec Chrome 81+, Safari 13+, Firefox 75+.<br>
✅ Compatible Mobile et Tablette iOS & Android.
<br><br>

## ℹ️  Informations

Si vous avez des questions, possible amélioration, ou encore si vous souhaitez contribuer à ce projet, surtout n'hésitez pas contactez-moi 📧 [Emilien.Chaptard@etu.uca.fr](mailto:emilien.chaptard@etu.uca.fr) avec le sujet "MMI Smart".<br>

## 🔨  Bientôt

<ul>
<li>Possibilité de partager avec le professeur le résultats</li>
<li>Ajout des boutons pour l'aide en CCS, PHP et JavaScript</li>
<li>Test de compatibilité avec les autres navigateurs web</li>
<ul><br>


# Exemple

Le but étant d'aider dans un premier temps l'apprentisage du langage de programmation HTML.

Pour avoir un aperçu d'une utilisation, voici un exexcice d'initiation au langage de programmation HTML 

[<img src="https://emchaptard.github.io/data/mmi-gif-new.gif" alt="GIF Demo MMI Smart" width="70%">](https://emchaptard.github.io/data/videommi.mov)<br>
[Voir la DEMO Vidéo en HD](https://emchaptard.github.io/data/videommi.mov).<br>

🔗 [DEMO Dynamique : Exemple complet ici](https://jsfiddle.net/emiliencode/fmwnye4h/21/).

# Licence

🔗 [Projet sous 'MIT License'](https://gitlab.com/emilienchaptard/mmi-smart/-/blob/master/LICENSE).
